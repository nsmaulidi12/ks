export default (context, inject) => {
	const setCurrency = number => {
		return 'Rp. ' + Number(number).toLocaleString('id-ID')
	}
	const catch404 = {
		statusCode: 404,
		message: 'Page Not Found'
	}
	const catch500 = {
		statusCode: 503,
		message: 'Error Code'
	}
	const add = (a, b) => {
		return a + b
	}

	const sum = arr => {
		return arr.reduce(add)
	}

	const dataImg = (img, size) => {
		let image

		if (img === undefined) {
			image = `/img/default-${size}.png`
		} else {
			image = img[0].conversion[size]
		}

		return image
	}

	const sanitizeQstring = q => {
		return q.replace(/[^=&]+=(&|$)/g, '').replace(/&$/, '')
	}

	const isEmptyObj = Obj => {
		return Object.keys(Obj).length === 0 && Obj.constructor === Object
	}

	const scrollTop = () => {
		window.scrollTo(0, 0)
	}

	const SEOMeta = ({ metaTitle, metaImage, metaDesc, metaKeywords }) => {
		const contentImage = metaImage || '/icon.png'
		const contentDesc = metaDesc || process.env.npm_package_description
		return {
			title: metaTitle,
			meta: [
				{
					hid: 'description',
					name: 'description',
					content: contentDesc
				},
				{
					hid: 'og:title',
					property: 'og:title',
					content: metaTitle
				},
				{
					hid: 'og:description',
					property: 'og:description',
					content: contentDesc
				},
				{
					hid: 'og:image',
					property: 'og:image',
					content: contentImage
				},
				{
					hid: 'twitter:title',
					name: 'twitter:title',
					content: metaTitle
				},
				{
					hid: 'twitter:description',
					name: 'twitter:description',
					content: contentDesc
				},
				{
					hid: 'twitter:image',
					name: 'twitter:image',
					content: contentImage
				},
				{
					hid: 'keywords',
					name: 'keywords',
					content: metaKeywords || ''
				}
			]
		}
	}

	const StrongPassword = (min, password) => {
		return (
			/[a-z]/.test(password) && // checks for a-z
			/[A-Z]/.test(password) && // checks for a-z
			/[0-9]/.test(password) && // checks for 0-9
			/\W|_/.test(password) && // checks for special char
			password.length >= 8
		)
	}
	const formatDate = (
		date,
		type = 'dash',
		format = 'dayFirst',
		withTime = true
	) => {
		try {
			let now
			if (date.toString().includes('T')) {
				now = new Date(date)
			} else {
				now = date.toString().includes('-')
					? new Date(date.replace(/-/g, '/'))
					: new Date(date)
			}
			const year = now.getFullYear()
			let month = now.getMonth() + 1
			let day = now.getDate()
			let hour = now.getHours()
			let minute = now.getMinutes()
			let second = now.getSeconds()
			if (month.toString().length === 1) {
				month = '0' + month
			}
			if (day.toString().length === 1) {
				day = '0' + day
			}
			if (hour.toString().length === 1) {
				hour = '0' + hour
			}
			if (minute.toString().length === 1) {
				minute = '0' + minute
			}
			if (second.toString().length === 1) {
				second = '0' + second
			}
			let first
			let last
			let delimiter
			if (format === 'dayFirst') {
				first = day
				last = year
			} else {
				first = year
				last = day
			}
			type === 'dash' ? (delimiter = '-') : (delimiter = '/')

			let dateTime = `${first}${delimiter}${month}${delimiter}${last}`
			if (withTime) dateTime = `${dateTime} ${hour}:${minute}:${second}`
			return dateTime
		} catch {
			return 'Invalid Date'
		}
	}
	const arrangeQuery = query => {
		let counter = 0
		let result = ''
		query.forEach(item => {
			if (item !== '') {
				result += `${counter > 0 ? '&' : ''}${item}`
				counter++
			}
		})
		return result
	}
	// must be object not array
	const createFilter = (obj, options) => {
		let filterString = ''
		let counter = 0
		if (Object.keys(obj).length) {
			Object.keys(obj).forEach(key => {
				if (
					(Array.isArray(obj[key]) && obj[key].length) ||
					(!Array.isArray(obj[key]) && obj[key])
				) {
					if (typeof obj[key] === 'object') {
						Object.keys(obj[key]).forEach(key1 => {
							if (
								(Array.isArray(obj[key][key1]) && obj[key][key1].length) ||
								(!Array.isArray(obj[key][key1]) && obj[key][key1])
							) {
								let value = obj[key][key1]
								if (options?.encode) value = encodeURIComponent(value)

								filterString += `${
									counter > 0 ? '&' : ''
								}filter[${key}.${key1}]=${value}`
								counter++
							}
						})
					} else {
						let value = obj[key]
						if (options?.encode) value = encodeURIComponent(value)

						filterString += `${counter > 0 ? '&' : ''}filter[${key}]=${value}`
						counter++
					}
				}
			})
		}
		return filterString
	}
	inject('createFilter', createFilter)
	inject('arrangeQuery', arrangeQuery)
	inject('formatDate', formatDate)
	inject('setCurrency', setCurrency)
	inject('catch404', catch404)
	inject('catch500', catch500)
	inject('sum', sum)
	inject('dataImg', dataImg)
	inject('isEmptyObj', isEmptyObj)
	inject('sanitizeQstring', sanitizeQstring)
	inject('scrollTop', scrollTop)
	inject('SEOMeta', SEOMeta)
	inject('StrongPassword', StrongPassword)
}

export function isEmpty(data) {
	return (data || []).length === 0 || Object.keys(data).length === 0
}
