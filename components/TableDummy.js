const data = [
	{
		id: 223,
		name: 'Group promo',
		description: 'Group promo',
		status: 'active',
		start_date: '2022-08-18 00:00:00',
		end_date: '2022-08-18 23:59:59',
		discount_amount: '250000.00',
		created_by: 'head-office'
	},
	{
		id: 222,
		name: 'Max Value 18',
		description: 'Max value',
		status: 'active',
		start_date: '2022-08-18 00:00:00',
		end_date: '2022-08-20 23:59:59',
		discount_amount: null,
		created_by: 'head-office'
	},
	{
		id: 221,
		name: 'Max Value',
		description: 'Max Value',
		status: 'cancelled',
		start_date: '2022-08-18 00:00:00',
		end_date: '2022-08-20 23:59:59',
		discount_amount: null,
		created_by: 'head-office'
	},
	{
		id: 219,
		name: 'Test day and time',
		description: 'Test day and time',
		status: 'active',
		start_date: '2022-08-18 00:00:00',
		end_date: '2022-08-25 23:59:59',
		discount_amount: '5000.00',
		created_by: 'store-leader'
	},
	{
		id: 218,
		name: 'Loadchare BCA 3',
		description: 'Loadchare BCA 3',
		status: 'active',
		start_date: '2022-08-16 00:00:00',
		end_date: '2022-08-30 23:59:59',
		discount_amount: null,
		created_by: 'head-office'
	},
	{
		id: 217,
		name: 'loadshared BCA 2',
		description: 'loadshared BCA 2',
		status: 'cancelled',
		start_date: '2022-08-16 00:00:00',
		end_date: '2022-08-30 23:59:59',
		discount_amount: null,
		created_by: 'head-office'
	},
	{
		id: 216,
		name: 'Loadshare BCA',
		description: 'Loadshare BCA',
		status: 'active',
		start_date: '2022-08-16 00:00:00',
		end_date: '2022-08-31 23:59:59',
		discount_amount: null,
		created_by: 'head-office'
	},
	{
		id: 215,
		name: 'Loadshare BCA',
		description: 'Loadshare BCA',
		status: 'cancelled',
		start_date: '2022-08-16 00:00:00',
		end_date: '2022-09-06 23:59:59',
		discount_amount: null,
		created_by: 'head-office'
	},
	{
		id: 214,
		name: 'Loadshared Voucher BCA',
		description: 'Loadshared Voucher BCA',
		status: 'cancelled',
		start_date: '2022-08-16 00:00:00',
		end_date: '2022-08-31 23:59:59',
		discount_amount: null,
		created_by: 'head-office'
	},
	{
		id: 213,
		name: 'test loadshare',
		description: 'test loadshare',
		status: 'cancelled',
		start_date: '2022-08-16 00:00:00',
		end_date: '2022-08-30 23:59:59',
		discount_amount: null,
		created_by: 'head-office'
	}
]
const metaData = {
	current_page: 1,
	from: 1,
	last_page: 21,
	links: [
		{ url: null, label: '&laquo; Previous', active: false },
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=1',
			label: '1',
			active: true
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=2',
			label: '2',
			active: false
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=3',
			label: '3',
			active: false
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=4',
			label: '4',
			active: false
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=5',
			label: '5',
			active: false
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=6',
			label: '6',
			active: false
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=7',
			label: '7',
			active: false
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=8',
			label: '8',
			active: false
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=9',
			label: '9',
			active: false
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=10',
			label: '10',
			active: false
		},
		{ url: null, label: '...', active: false },
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=20',
			label: '20',
			active: false
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=21',
			label: '21',
			active: false
		},
		{
			url: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=2',
			label: 'Next &raquo;',
			active: false
		}
	],
	path: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions',
	per_page: 10,
	to: 10,
	total: 209
}
const linksData = {
	first:
		'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=1',
	last: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=21',
	prev: null,
	next: 'https://snowy-prague-ltu7inqjjxtk.vapor-farm-a1.com/api/v1/promotions?page%5Bsize%5D=10&sort=-created_at&page%5Bnumber%5D=2'
}
export default {
	data,
	metaData,
	linksData
}
